/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package regources

type AtomicSwapAskRelationships struct {
	BaseBalance *Relation           `json:"base_balance,omitempty"`
	Owner       *Relation           `json:"owner,omitempty"`
	QuoteAssets *RelationCollection `json:"quote_assets,omitempty"`
}
