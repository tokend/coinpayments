FROM golang:1.12

ARG VERSION="dirty"
WORKDIR /go/src/gitlab.com/tokend/coinpayments
COPY . .
RUN CGO_ENABLED=0 \
    GOOS=linux \
    go build -ldflags "-X gitlab.com/tokend/coinpayments/internal/config.CoinpaymentsRelease=${VERSION}" -o /coinpayments -v gitlab.com/tokend/coinpayments

FROM alpine:3.9
COPY --from=0 /coinpayments /usr/local/bin/coinpayments
RUN apk add --no-cache ca-certificates
ENTRYPOINT ["coinpayments", "run"]
