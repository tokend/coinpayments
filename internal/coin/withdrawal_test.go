package coin

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConnector_GetWithdrawalInfo(t *testing.T) {
	t.Run("good response", func(t *testing.T) {
		ts := prepareTestServer()
		defer ts.Close()

		cc := Connector{
			config: Config{Address: ts.URL, PrivateKey: "private_key", PublicKey: "public_key", IPNSecret: "ipn_secret", RequestTimeout: 1000},
			client: http.DefaultClient,
		}

		details, err := cc.GetWithdrawalInfo("wid")
		assert.NoError(t, err)
		assert.NotNil(t, details)
	})

	t.Run("error response", func(t *testing.T) {
		ts := prepareFaultyTestServer()
		defer ts.Close()

		cc := Connector{
			config: Config{Address: ts.URL, PrivateKey: "private_key", PublicKey: "public_key", IPNSecret: "ipn_secret", RequestTimeout: 1000},
			client: http.DefaultClient,
		}

		details, err := cc.GetWithdrawalInfo("wid")
		assert.Error(t, err)
		assert.Nil(t, details)
	})
}

func TestConnector_CreateWithdrawal(t *testing.T) {
	t.Run("good response", func(t *testing.T) {
		ts := prepareTestServer()
		defer ts.Close()

		cc := Connector{
			config: Config{Address: ts.URL, PrivateKey: "private_key", PublicKey: "public_key", IPNSecret: "ipn_secret", RequestTimeout: 1000},
			client: http.DefaultClient,
		}

		w := Withdrawal{
			Address:  "bc1SomeAddress",
			Currency: "BTC",
			Amount:   "1.000032",
		}

		details, err := cc.CreateWithdrawal(w)
		assert.NoError(t, err)
		assert.NotNil(t, details)
		assert.Equal(t, w.Amount, details.Amount.String())
	})

	t.Run("error response", func(t *testing.T) {
		ts := prepareFaultyTestServer()
		defer ts.Close()

		cc := Connector{
			config: Config{Address: ts.URL, PrivateKey: "private_key", PublicKey: "public_key", IPNSecret: "ipn_secret", RequestTimeout: 1000},
			client: http.DefaultClient,
		}

		w := Withdrawal{
			Address:  "bc1SomeAddress",
			Currency: "BTC",
			Amount:   "1.00003212",
		}

		details, err := cc.CreateWithdrawal(w)
		assert.Error(t, err)
		assert.Nil(t, details)
	})
}
