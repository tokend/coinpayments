package coin

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha512"
	"encoding/hex"
	"encoding/json"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"io/ioutil"
)

const (
	apiVersion        = "1"
	createWithdrawal  = "create_withdrawal"
	getWithdrawalInfo = "get_withdrawal_info"
	getTxInfoMulti    = "get_tx_info_multi"
	getTxInfo         = "get_tx_info"
	createTransaction = "create_transaction"
)

// Connector for Coinpayments
type Connector struct {
	config Config
	client *http.Client
}

func (c *Connector) performRequest(data url.Values, v interface{}) error {
	urlRaw, r, err := c.prepareRequest(data)
	if err != nil {
		return errors.Wrap(err, "failed to prepare request")
	}
	fields := logan.F{
		"url_raw": urlRaw,
		"request": r,
	}

	resp, err := c.sendRequest(r, urlRaw)
	if err != nil {
		return errors.Wrap(err, "failed to send request", fields)
	}

	respBuffer := new(bytes.Buffer)
	_, err = respBuffer.ReadFrom(resp)
	if err != nil {
		return errors.Wrap(err, "failed to read response body", fields)
	}

	respBB := respBuffer.Bytes()

	e := errField{}
	err = json.Unmarshal(respBB, &e)
	if err != nil {
		return errors.Wrap(err, "failed to unmarshal response body to JSON with error", fields.Merge(logan.F{
			"raw_response": string(respBB),
		}))
	}
	if e.Error != "ok" {
		return errors.Wrap(errors.New(e.Error), "got error from Coinpayments", fields)
	}

	err = json.Unmarshal(respBB, v)
	if err != nil {
		return errors.Wrap(err, "failed to unmarshal response body to JSON", fields.Merge(logan.F{
			"raw_response": respBB,
		}))
	}

	return nil
}

//NewConnector prepares new Coinpayments' connector off of provided config
func NewConnector(config Config) *Connector {
	return &Connector{
		config: config,
		client: &http.Client{
			Timeout: time.Second * time.Duration(config.RequestTimeout),
		},
	}
}

func (c *Connector) generateAuthKey(requestData string) (string, error) {
	mac := hmac.New(sha512.New, []byte(c.config.PrivateKey))

	_, err := mac.Write([]byte(requestData))
	if err != nil {
		return "", errors.Wrap(err, "failed to generate Auth Key")
	}

	return hex.EncodeToString(mac.Sum(nil)), nil
}

// TODO Make sure same things should go into url and body..
func (c *Connector) prepareRequest(requestData url.Values) (string, *http.Request, error) {
	u, err := url.ParseRequestURI(c.config.Address)
	if err != nil {
		return "", nil, errors.Wrap(err, "failed to parse request URI")
	}
	urlStr := u.String()

	requestData.Add("key", c.config.PublicKey)
	requestData.Add("version", apiVersion)

	urlRaw := requestData.Encode()
	r, err := http.NewRequest("POST", urlStr, strings.NewReader(urlRaw))
	if err != nil {
		return "", nil, errors.Wrap(err, "Failed to create http request")
	}

	authKey, err := c.generateAuthKey(urlRaw)
	if err != nil {
		return "", nil, errors.Wrap(err, "failed to generate AuthKey")
	}

	r.Header.Set("HMAC", authKey)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	return urlRaw, r, nil
}

func (c *Connector) sendRequest(r *http.Request, urlRaw string) (io.ReadCloser, error) {
	resp, err := c.client.Do(r)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to send http POST request")
	}

	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		fields := logan.F{
			"status_code": resp.StatusCode,
		}

		rawResponseBB, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fields["reading_raw_response_err"] = err
		} else {
			fields["raw_response"] = string(rawResponseBB)
		}

		return nil, errors.From(errors.New("Unsuccessful response from coinPayment"), fields)
	}

	return resp.Body, nil
}
