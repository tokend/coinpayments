package coin

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
)

func prepareTestServer() *httptest.Server {
	return httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		body := r.Body
		buf := new(bytes.Buffer)
		buf.ReadFrom(body)

		data := buf.String()
		args := strings.Split(data, "&")
		argMap := make(map[string]string)
		for _, v := range args {
			kv := strings.Split(v, "=")
			argMap[kv[0]] = kv[1]
		}

		switch argMap["cmd"] {
		case createTransaction:
			w.Write(json.RawMessage(fmt.Sprintf(createTxResp, argMap["amount"], argMap["address"], argMap["key"], argMap["key"])))
		case createWithdrawal:
			w.Write(json.RawMessage(fmt.Sprintf(createWithdrawResp, argMap["amount"])))
		case getWithdrawalInfo:
			w.Write(json.RawMessage(withdrawInfoResp))
		default:
			w.Write([]byte("Hello, vasil"))
		}
	}))
}

func prepareFaultyTestServer() *httptest.Server {
	return httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		body := r.Body
		buf := new(bytes.Buffer)
		buf.ReadFrom(body)

		data := buf.String()
		args := strings.Split(data, "&")
		argMap := make(map[string]string)
		for _, v := range args {
			kv := strings.Split(v, "=")
			argMap[kv[0]] = kv[1]
		}

		w.Write(json.RawMessage(errorResp))
	}))
}
