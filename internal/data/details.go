package data

type ExternalDetails struct {
	Invoice Invoice `json:"invoice"`
}

type Invoice struct {
	Address        string `json:"address"`
	Amount         string `json:"amount"`
	Asset          string `json:"asset"`
	TxID           string `json:"tx_id"`
	Timeout        int32  `json:"timeout"`
	ConfirmsNeeded string `json:"confirms_needed"`
}
