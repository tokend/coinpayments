package data

import (
	"encoding/json"
	"fmt"

	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/horizon-connector"
	regources "gitlab.com/tokend/regources/generated"
)

type AtomicSwapRequestProvider interface {
	GetAtomicSwapAskRequest(id string) (*regources.AtomicSwapAskResponse, error)
	GetAtomicSwapBidRequest(id string) (*regources.ReviewableRequestResponse, error)
}

type AtomicSwapConnector struct {
	client *horizon.Client
}

func NewAtomicSwapConnector(client *horizon.Client) *AtomicSwapConnector {
	return &AtomicSwapConnector{
		client: client,
	}
}

func (c *AtomicSwapConnector) GetAtomicSwapAskRequest(id string) (*regources.AtomicSwapAskResponse, error) {
	endpoint := fmt.Sprintf("/v3/atomic_swap_asks/%s?include=quote_assets", id)
	response, err := c.client.Get(endpoint)
	if err != nil {
		return nil, errors.Wrap(err, "request failed")
	}

	if response == nil {
		return nil, nil
	}

	var result regources.AtomicSwapAskResponse
	if err := json.Unmarshal(response, &result); err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal")
	}

	return &result, nil
}

func (c *AtomicSwapConnector) GetAtomicSwapBidRequest(id string) (*regources.ReviewableRequestResponse, error) {
	endpoint := fmt.Sprintf("/v3/create_atomic_swap_bid_requests/%s", id)
	response, err := c.client.Get(endpoint)
	if err != nil {
		return nil, errors.Wrap(err, "request failed")
	}

	if response == nil {
		return nil, nil
	}

	var result regources.ReviewableRequestResponse
	if err := json.Unmarshal(response, &result); err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal")
	}

	return &result, nil
}
