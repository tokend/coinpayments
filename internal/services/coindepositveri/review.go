package coindepositveri

import (
	"context"

	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/go/xdr"
	"gitlab.com/tokend/go/xdrbuild"
	"gitlab.com/tokend/regources"
)

func (s *Service) permanentlyRejectRequest(ctx context.Context, request *regources.ReviewableRequest, rejectReason string) error {
	envelope, err := s.builder.
		Transaction(s.source).
		Op(xdrbuild.ReviewRequest{
			ID:      request.ID,
			Hash:    &request.Hash,
			Action:  xdr.ReviewRequestOpActionPermanentReject,
			Reason:  rejectReason,
			Details: xdrbuild.IssuanceDetails{},
			ReviewDetails: xdrbuild.ReviewDetails{
				TasksToAdd: 0,
				TasksToRemove: 0,
				ExternalDetails: `{}`,
			},
		}).
		Sign(s.signer).Marshal()
	if err != nil {
		return errors.Wrap(err, "failed to marshal tx")
	}

	result := s.submitter.Submit(ctx, envelope)
	if result.Err != nil {
		return errors.Wrap(result.Err, "failed to submit tx", result.GetLoganFields())
	}

	return nil
}

func (s *Service) approveRequest(ctx context.Context, request *regources.ReviewableRequest) error {
	envelope, err := s.builder.
		Transaction(s.source).
		Op(xdrbuild.ReviewRequest{
			ID:      request.ID,
			Hash:    &request.Hash,
			Action:  xdr.ReviewRequestOpActionApprove,
			// IssuanceDetails does not have fields, just providing the proper type of the Details.
			Details: xdrbuild.IssuanceDetails{},
			ReviewDetails: xdrbuild.ReviewDetails{
				TasksToRemove:   taskVerifyDeposit,
				ExternalDetails: `{}`,
			},
		}).
		Sign(s.signer).Marshal()
	if err != nil {
		return errors.Wrap(err, "failed to marshal tx")
	}

	result := s.submitter.Submit(ctx, envelope)
	if result.Err != nil {
		return errors.Wrap(result.Err, "failed to submit tx", result.GetLoganFields())
	}

	return nil
}
