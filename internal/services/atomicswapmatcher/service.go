package atomicswapmatcher

import (
	"context"
	"encoding/json"
	"strconv"
	"strings"
	"time"

	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/go/xdr"
	regources "gitlab.com/tokend/regources/generated"

	"gitlab.com/tokend/coinpayments/internal/coin"

	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/running"
	"gitlab.com/tokend/coinpayments/internal/data"
	"gitlab.com/tokend/go/xdrbuild"
	"gitlab.com/tokend/horizon-connector"
	"gitlab.com/tokend/keypair"
)

type Service struct {
	log                       *logan.Entry
	reviewableRequestStreamer *data.ReviewableRequestStreamer
	signer                    keypair.Full
	excludedAssets            []string

	builder          *xdrbuild.Builder
	identityProvider data.IdentityProvider
	coinpayments     *coin.Connector
	horizon          *horizon.Connector
}

func New(
	log *logan.Entry,
	reviewableRequestStreamer *data.ReviewableRequestStreamer,
	signer keypair.Full,
	builder *xdrbuild.Builder,
	identityProvider data.IdentityProvider,
	coinpayments *coin.Connector,
	horizon *horizon.Connector,
	excludedAssets []string,
) *Service {
	return &Service{
		log:                       log,
		reviewableRequestStreamer: reviewableRequestStreamer,
		signer:                    signer,
		builder:                   builder,
		identityProvider:          identityProvider,
		coinpayments:              coinpayments,
		horizon:                   horizon,
		excludedAssets:            excludedAssets,
	}
}

func (s *Service) Run(ctx context.Context) {
	s.log.Info("Starting.")

	running.WithBackOff(ctx, s.log, "atomic-swap-matcher", s.startMatcher, 1*time.Second, 10*time.Second, 10*time.Second)
}

func (s *Service) startMatcher(ctx context.Context) error {
	streamer := s.reviewableRequestStreamer.New().
		WithState(data.FilterStatePending).
		WithPendingTasks(data.TaskPendingForTx)
	requestsStream := streamer.Stream()
	errs := streamer.Errors()

	for {
		if running.IsCancelled(ctx) {
			return nil
		}

		select {
		case requests := <-requestsStream:
			for _, request := range requests.Data {
				details := requests.Included.MustCreateAtomicSwapBidRequest(request.Relationships.RequestDetails.Data.GetKey())
				if details == nil || s.isExcluded(*details) {
					continue
				}

				if err := s.processRequest(ctx, request); err != nil {
					s.log.WithError(err).WithFields(logan.F{
						"request_id": request.ID,
						"request":    request,
					}).Error("failed to process request")
					continue
				}
			}
		case err := <-errs:
			if err == data.ErrEmptyPage {
				return nil
			}
			return errors.Wrap(err, "reviewable request streamer send error")
		}
	}
}

func (s *Service) processRequest(ctx context.Context, request regources.ReviewableRequest) error {
	if request.Attributes.XdrType != xdr.ReviewableRequestTypeCreateAtomicSwapBid {
		return nil
	}

	// normally error should never happen
	requestID, err := strconv.Atoi(request.ID)
	if err != nil {
		return errors.Wrap(err, "failed to convert request id")
	}

	txID, err := s.getTxID(request)
	if err != nil {
		s.log.WithError(err).WithField("request_id", requestID).Error("failed to get transaction id")
		return s.handleErrWithReject(ctx, data.ErrInternalProblems, requestID, &request)
	}
	txInfo, err := s.coinpayments.GetTxInfo(txID)
	if err != nil {
		s.log.WithField("tx_id", txID).Debug("failed to get tx info from coinpayments")
		return nil
	}

	switch txInfo.Status {
	case 0: // pending
		s.log.WithField("tx_id", txInfo.ID).Debug("skipping pending tx")
	case -1: // t/o
		return s.handleErrWithReject(ctx, data.ErrCanceledRequest, requestID, &request)
	case 1: // got confirm
		s.log.WithField("tx_id", txInfo.ID).Debug(txInfo.StatusText)
	case 100: // ok
		xdrTx, err := s.builder.Transaction(s.signer).Op(xdrbuild.ReviewRequest{
			ID:      uint64(requestID),
			Hash:    &request.Attributes.Hash,
			Details: xdrbuild.AtomicSwapDetails{},
			Action:  xdr.ReviewRequestOpActionApprove,
			ReviewDetails: xdrbuild.ReviewDetails{
				TasksToAdd:    0,
				TasksToRemove: data.TaskPendingForTx,
			},
		}).Sign(s.signer).Marshal()
		if err != nil {
			return errors.Wrap(err, "Error create Envelope")
		}
		submitResult := s.horizon.Submitter().Submit(ctx, xdrTx)
		if submitResult.Err != nil {
			return errors.Wrap(submitResult.Err, "Error submitting signed Envelope to Horizon", logan.F{
				"submit_result": submitResult,
			})
		}
	}

	return nil
}

func (s *Service) isExcluded(request regources.CreateAtomicSwapBidRequest) bool {
	quoteAsset := strings.Split(request.Relationships.QuoteAsset.Data.ID, ":")[0]
	for _, asset := range s.excludedAssets {
		if asset == quoteAsset {
			return true
		}
	}
	return false
}

// FIXME unmarshal invoice
func (s *Service) getTxID(request regources.ReviewableRequest) (string, error) {
	var externalDetails struct {
		Data []data.Invoice `json:"data"`
	}

	err := json.Unmarshal(request.Attributes.ExternalDetails, &externalDetails)
	if err != nil {
		return "", errors.Wrap(err, "failed to unmarshal external details")
	}
	if len(externalDetails.Data) == 0 {
		return "", errors.New("failed to unmarshal external details")
	}

	return externalDetails.Data[0].TxID, nil
}

func (s *Service) handleErrWithReject(ctx context.Context, err error, requestID int, request *regources.ReviewableRequest) error {
	var rejectReason string
	switch err.(type) {
	case data.Error:
		rejectReason = err.Error()
	default:
		rejectReason = data.ErrInternalProblems.Error()
	}
	rejectErr := s.rejectRequest(ctx, requestID, request, rejectReason)
	if rejectErr != nil {
		return errors.Wrap(err, "failed to reject request")
	}
	return err
}

func (s *Service) rejectRequest(ctx context.Context, requestID int, request *regources.ReviewableRequest, rejectReason string) error {
	envelope, err := s.builder.Transaction(s.signer).Op(xdrbuild.ReviewRequest{
		ID:      uint64(requestID),
		Hash:    &request.Attributes.Hash,
		Details: xdrbuild.AtomicSwapDetails{},
		Action:  xdr.ReviewRequestOpActionPermanentReject,
		ReviewDetails: xdrbuild.ReviewDetails{
			TasksToAdd:    data.TaskPendingForTx,
			TasksToRemove: 0,
		},
		Reason: rejectReason,
	}).Sign(s.signer).Marshal()
	if err != nil {
		return errors.Wrap(err, "failed to marshal tx")
	}

	submitResult := s.horizon.Submitter().Submit(ctx, envelope)
	if submitResult.Err != nil {
		return errors.Wrap(submitResult.Err, "failed to submit envelope", logan.F{
			"submit_result": submitResult,
		})
	}
	return nil
}
