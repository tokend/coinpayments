package coindeposit

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/distributed_lab/ape/apeutil"
)

func TestRequest_Validate(t *testing.T) {
	cases := []struct {
		body     string
		expected *Request
	}{
		{ // valid
			`{
				"data": {
					"attributes": {
						"amount": "13.37",
						"receiver": "BDRDDBDJE34YUXD6TX255BXDBWLX4MAHBZR6M5YEZ47X5244ZXHCXBDZ"
					}
				}
			}`,
			&Request{
				Data: RequestData{
					Attributes: RequestAttributes{
						Amount:   "13.37",
						Receiver: "BDRDDBDJE34YUXD6TX255BXDBWLX4MAHBZR6M5YEZ47X5244ZXHCXBDZ",
					},
				},
			},
		},
		{ // missing amount
			`{
				"data": {
					"attributes": {
						"receiver": "BDRDDBDJE34YUXD6TX255BXDBWLX4MAHBZR6M5YEZ47X5244ZXHCXBDZ"
					}
				}
			}`,
			nil,
		},
		{ // invalid amount
			`{
				"data": {
					"attributes": {
						"amount": "oopsie",
						"receiver": "BDRDDBDJE34YUXD6TX255BXDBWLX4MAHBZR6M5YEZ47X5244ZXHCXBDZ"
					}
				}
			}`,
			nil,
		},
		{ // missing receiver
			`{
				"data": {
					"attributes": {
						"receiver": "BDRDDBDJE34YUXD6TX255BXDBWLX4MAHBZR6M5YEZ47X5244ZXHCXBDZ"
					}
				}
			}`,
			nil,
		},
	}

	for idx, tc := range cases {
		t.Run(fmt.Sprintf("%d", idx), func(t *testing.T) {
			got, err := NewRequest(apeutil.RequestWithURLParams([]byte(tc.body), nil))
			if tc.expected == nil {
				assert.Error(t, err)
			} else {
				assert.Equal(t, *tc.expected, got)
			}
		})
	}
}
