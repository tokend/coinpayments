package coindeposit

import (
	"context"
	"encoding/hex"
	"encoding/json"

	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/coinpayments/internal/coin"
	"gitlab.com/tokend/coinpayments/internal/horizonconnector"
	"gitlab.com/tokend/go/amount"
	"gitlab.com/tokend/go/hash"
	"gitlab.com/tokend/go/xdrbuild"
)

// GetAssetCode returns error if Balance was not found for the Account.
func (s *Service) getAssetCode(accountID, balanceID string) (string, error) {
	balances, err := s.accounts.Balances(accountID)
	if err != nil {
		return "", errors.Wrap(err, "failed to get Balances for AccountID")
	}
	if len(balances) == 0 {
		return "", errors.New("no Balances were found for the Account")
	}

	for _, b := range balances {
		if b.BalanceID == balanceID {
			return b.Asset, nil
		}
	}

	return "", errors.From(errors.New("such Balance was not found for the Account"), logan.F{
		"balances": balances,
	})
}

func (s *Service) processIssuance(ctx context.Context, attrs RequestAttributes, assetCode string, details *coin.TxDetails) error {
	am, err := amount.Parse(attrs.Amount)
	if err != nil {
		return errors.Wrap(err, "failed to parse amount", logan.F{
			"amount": attrs.Amount,
		})
	}

	if am < 0 {
		// FIXME Wrapping of new error looks weird
		return errors.Wrap(errors.New("can't have amount < 0"),
			"invalid amount",
			logan.F{
				"amount": attrs.Amount,
			})
	}

	b, err := json.Marshal(details)
	if err != nil {
		return errors.Wrap(err, "failed to marshal details", logan.F{
			"details": details,
		})
	}
	refHash := hash.Hash([]byte(details.TxnID))

	reference := hex.EncodeToString(refHash[:])
	envelope, err := s.builder.Transaction(s.source).Op(&xdrbuild.CreateIssuanceRequest{
		Asset:     assetCode,
		Amount:    uint64(am),
		AllTasks:  &defaultIssuanceTasks,
		Receiver:  attrs.Receiver,
		Details:   json.RawMessage(b),
		Reference: reference,
	}).Sign(s.signer).Marshal()
	if err != nil {
		return errors.Wrap(err, "failed to build tx envelope")
	}

	ok, err := SubmitEnvelope(ctx, envelope, s.submitter)
	if err != nil {
		return errors.Wrap(err, "failed to submit issuance request")
	}

	if !ok {
		s.log.Warn("Reference duplication, something bad happened")
		return errors.Wrap(errors.New("failed to submit issuance request"),
			"Unexpected state already have issuance request with same Coinpayments' txID",
			logan.F{
				"tx_id": details.TxnID,
			})

	}
	return nil
}

func (s *Service) getIdentity(balance string) (*Identity, error) {
	accountID, err := s.balances.AccountID(balance)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get AccountID for given BalanceID", logan.F{
			"balance_id": balance,
		})
	}
	if accountID == nil {
		return nil, nil
	}
	fields := logan.F{
		"account_id": *accountID,
	}

	identity, err := s.identities.GetIdentity(*accountID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get Identity by Address", fields)
	}
	if identity == nil {
		return nil, nil
	}

	return identity, nil
}

// GetCoinpaymentsAssetCode returns nil,nil if the Asset does not exist or is not a Coinpayments asset.
func (s *Service) getCoinpaymentsAssetCode(accountID, balance string) (*string, error) {
	code, err := s.getAssetCode(accountID, balance)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get asset code")
	}

	asset, err := s.assets.ByCode(code)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get asset by code", logan.F{
			"asset_code": code,
		})
	}
	if asset == nil {
		// Asset with such AssetCode does not exist.
		return nil, nil
	}

	if !s.isCoinpayments(*asset) {
		return nil, nil
	}

	return &code, nil
}

// This is method, not a function because implementation could potentially change and a new one would require the Service to be accessible.
func (s *Service) isCoinpayments(asset horizonconnector.Asset) bool {
	return asset.Details.IsCoinpayments
}

func (s *Service) createCoinpaymentsTx(params RequestAttributes, asset string, email string) (*coin.TxDetails, error) {
	tx := coin.Transaction{
		Amount:            params.Amount,
		ReceivingCurrency: asset,
		SendingCurrency:   asset,
		Custom:            params.Receiver,
		BuyerEmail:        email,
	}

	fields := logan.F{
		"cp_tx_request": tx,
	}

	cpTxDetails, err := s.coinpayments.CreateTx(tx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create transaction with given parameters", fields)
	}
	fields = fields.Merge(logan.F{
		"cp_tx_response": cpTxDetails,
	})

	if len(cpTxDetails.TxnID) == 0 {
		return nil, errors.From(errors.New("coinpayments returned empty txn_id"), fields)
	}

	return cpTxDetails, nil
}
