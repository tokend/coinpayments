package coindeposit

import (
	"context"
	"net"
	"net/http"

	"gitlab.com/distributed_lab/kit/copus/types"

	"gitlab.com/tokend/coinpayments/internal/coin"

	"gitlab.com/tokend/keypair"

	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/tokend/coinpayments/internal/horizonconnector"
	"gitlab.com/tokend/go/xdrbuild"
	"gitlab.com/tokend/horizon-connector"
)

const (
	//taskVerifyDeposit is set on issuance request
	//in order to wait for deposit to be approved
	taskVerifyDeposit uint32 = 1024
)

var (
	//defaultIssuanceTasks are tasks by default set for new issuance
	//requests by this service
	defaultIssuanceTasks = taskVerifyDeposit
)

type IdentityProvider interface {
	// GetIdentity returns nil,nil if Identity was not found
	GetIdentity(address string) (*Identity, error)
}

type accountProvider interface {
	//User gets user by account ID
	Balances(accID string) ([]horizon.Balance, error)
}

type balanceProvider interface {
	//AccountID gets account ID by balance ID
	AccountID(balanceID string) (*string, error)
}

type PaymentService interface {
	CreateTx(transaction coin.Transaction) (*coin.TxDetails, error)
}

type AssetProvider interface {
	// ByCode returns nil,nil if the Asset with the AssetCode is not found.
	ByCode(code string) (*horizonconnector.Asset, error)
}

type TXSubmitter interface {
	Submit(ctx context.Context, envelope string) horizon.SubmitResult
}

// Service implements app.Service interface.
// and sends Issuance(CoinEmissionRequests) to Horizon.
type Service struct {
	log      *logan.Entry
	listener net.Listener
	cop      types.Copus

	source keypair.Address
	signer keypair.Full

	builder *xdrbuild.Builder

	assets     AssetProvider
	identities IdentityProvider
	balances   balanceProvider
	accounts   accountProvider
	submitter  TXSubmitter

	coinpayments PaymentService
}

// New is constructor for the coindeposit Service.
//
// Make sure txsubmitter, provided to constructor, is with signer.
func New(
	log *logan.Entry,
	listener net.Listener,
	cop types.Copus,
	source keypair.Address,
	signer keypair.Full,
	builder *xdrbuild.Builder,
	horizonConnector *horizon.Connector,
	coinpayments PaymentService,
) *Service {
	return &Service{
		log:          log.WithField("service", "coinpayments_deposit"),
		listener:     listener,
		cop:          cop,
		source:       source,
		signer:       signer,
		builder:      builder,
		coinpayments: coinpayments,

		identities: NewIdentityConnector(horizonConnector.Client()),
		submitter:  horizonConnector.Submitter(),
		balances:   horizonConnector.Balances(),
		assets:     horizonconnector.NewAssetConnector(horizonConnector.Client()),
		accounts:   horizonConnector.Accounts(),
	}
}

// Run is used to run coindeposit service which will listen for
// new deposit requests.
func (s *Service) Run(ctx context.Context) {
	s.log.Info("Starting.")

	r := s.prepareRouter()

	if err := http.Serve(s.listener, r); err != nil {
		panic(err)
	}
}
