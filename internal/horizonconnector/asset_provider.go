package horizonconnector

import (
	"encoding/json"
	"fmt"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/horizon-connector"
	"gitlab.com/tokend/regources"
)

type Asset struct {
	Code                 string           `json:"code"`
	Owner                string           `json:"owner"`
	AvailableForIssuance regources.Amount `json:"available_for_issuance"`
	Details              AssetDetails     `json:"details"`
	Issued               regources.Amount `json:"issued"`
	Type                 uint64           `json:"type"`
}

type AssetDetails struct {
	// ExternalSystemType supposed external system type used for deposit (int32)
	ExternalSystemType string `json:"external_system_type"`
	IsCoinpayments     bool   `json:"is_coinpayments"`
}

type AssetConnector struct {
	client *horizon.Client
}

func NewAssetConnector(client *horizon.Client) *AssetConnector {
	return &AssetConnector{
		client: client,
	}
}

func (c *AssetConnector) ByCode(code string) (*Asset, error) {
	endpoint := fmt.Sprintf("/assets/%s", code)
	response, err := c.client.Get(endpoint)
	if err != nil {
		return nil, errors.Wrap(err, "request failed")
	}

	if response == nil {
		return nil, nil
	}

	var asset Asset
	if err := json.Unmarshal(response, &asset); err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal response", logan.F{
			"raw_response": string(response),
		})
	}

	return &asset, nil
}
