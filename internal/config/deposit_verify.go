package config

import (
	"github.com/pkg/errors"
	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/tokend/keypair"
	"gitlab.com/tokend/keypair/figurekeypair"
)

type DepositVerifyConfig struct {
	Source keypair.Address `fig:"source,required"`
	Signer keypair.Full    `fig:"signer,required"`
}

func (c *config) DepositVerifyConfig() DepositVerifyConfig {
	return c.depositVerify.Do(func() interface{} {
		var result DepositVerifyConfig

		err := figure.
			Out(&result).
			With(figure.BaseHooks, figurekeypair.Hooks, URLHook).
			From(kv.MustGetStringMap(c.getter, "deposit_verify")).
			Please()
		if err != nil {
			panic(errors.Wrap(err, "failed to figure out deposit_verify"))
		}

		return result
	}).(DepositVerifyConfig)
}
